import java.util.LinkedList;

public class DoubleStack {

    public static void main(String[] argum) {
        String s = "-3 -5 -7 ROT - SWAP DUP * +";
//        DoubleStack.interpret (s);
    }

    private LinkedList<Double> doubleLinkedList;

    DoubleStack() {
        doubleLinkedList = new LinkedList<>();

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoubleStack ds = new DoubleStack();
        if (!stEmpty()) {
            ds.doubleLinkedList.addAll(doubleLinkedList);
        }
        return ds;
    }

    public boolean stEmpty() {
        return doubleLinkedList.isEmpty();
    }

    public void push(double a) {
        doubleLinkedList.addLast(a);
    }

    public double pop() {
        if (stEmpty()) throw new IndexOutOfBoundsException("Stack underflow");
        return doubleLinkedList.pollLast();
    } // pop

    public void op(String s) {
        if (doubleLinkedList.size() < 2) throw new IndexOutOfBoundsException("Too few elements to count for " + s);
        double second = pop();
        double first = pop();
        switch (s) {
            case "+":
                push(first + second);
                break;
            case "-":
                push(first - second);
                break;
            case "*":
                push(first * second);
                break;
            case "/":
                push(first / second);
                break;
            default:
                throw new IllegalArgumentException("Illegal argument: " + s);
        }
    }

    public double tos() {
        if (stEmpty()) throw new IndexOutOfBoundsException("Stack underflow");
        return doubleLinkedList.getLast();
    }

    @Override
    public boolean equals(Object o) {
        return doubleLinkedList.equals(((DoubleStack) o).doubleLinkedList);
    }

    @Override
    public String toString() {
        return doubleLinkedList.toString();
    }

    public static double interpret(String pol) {

        if (pol.isEmpty()) {
            throw new IllegalArgumentException("Empty expression");
        }

        DoubleStack ds = new DoubleStack();

        String[] pos = pol.split("\\s+");
        for (String str : pos) {
            if (str.isEmpty()) continue;
            try {
                ds.push(Double.parseDouble(str));
            } catch (NumberFormatException ne) {
                try {
                    switch (str.toUpperCase()){
                        case "SWAP":
                            double last = ds.pop();
                            double second_last = ds.pop();
                            ds.push(last);
                            ds.push(second_last);
                            break;
                        case "DUP":
                            if (!ds.doubleLinkedList.isEmpty()){
                                double a = ds.tos();
                                ds.push(a);
                            }
                            break;
                        case "ROT":
                            if (ds.doubleLinkedList.size() > 2){
                                double toLast = ds.doubleLinkedList.get(ds.doubleLinkedList.size() - 3);
                                ds.doubleLinkedList.remove(ds.doubleLinkedList.size() - 3);
                                ds.push(toLast);
                            }
                            break;
                        default:
                            ds.op(str);
                    }
                } catch ( IllegalArgumentException e){
                    throw new RuntimeException("Illegal argument " + str + " in expression \"" + pol + "\"");
                } catch (IndexOutOfBoundsException i){
                    throw new RuntimeException("Cannot perform " + str + " in expression \"" + pol + "\"");
                }
            }
        }
        if (ds.doubleLinkedList.size() != 1) {
            throw new RuntimeException("Too many numbers in expression \"" + pol + "\"");
        }
        return ds.tos();
    }
}